<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->

## Systemadministration
### Vorlesung

---
## From Infrastruktur as Code to the Cloud



---
## Agenda Cloud
* Cloud Einführung
* Eigenschaften von Cloudanwendungen
* Vier Charakteristika
* Drei Service Modelle
* Vier Deployment Modelle

---
## Cloud Definition
#### Ist ein Model um Standartisiert und sofort über ein Netzwerk (Internet) auf Resourcen (Commute / Storage / Network) zugreifen zu können.

##### (NIST 2009)

---
## Eigenschaften von Cloudanwendungen
* schnell Verfügbar (Rapidly Provisioned)
* minimaler Management Aufwand seitens des Providers
* wenig direkte Interaktion mit dem Service Providers

---
## Cloud Charakteristika
* On Demand , Self Service
* Network Accessable
* Resource Pooling
* Rapid Elasticity
* Measured Service

---
## Cloud Service Modelle
* Infrastruktur as a Service (IaaS)
* Plattform as a Service (PaaS)
* Software as a Service (SaaS)
* Function as a Service (FaaS)

---
## Infrastruktur as a Service
#### Der Serviceprovider stellt Teile seiner Infrastruktur in einem Abstrahierten / Virtualisierten Mandantenfähigen System zur Verfügung

---
## Infrastruktur as a Service
* Der Kunde bekommt mit diesem Modell die größte Flexiblität

---
## Infrastruktur as a Service
* Infrastruktur as Code
* Software Defined Datacenter und
* Multitennend Datacenter

---
## Beispiele
* Virtuelle Maschinen
* Container
* Storage
* Netzwerkappliances

---
## Marktübersicht
* Amazon - EC2 (AWS)
* Azure
* DigitalOcean
* Dreamhost
* Rackspace

---
## Plattfrom as a Service
#### Ein Softwarestack wird meist in einer Standartisierten Umgebung angeboten

---
## Plattfrom as a Service
* Der Kunde kann seine selbst für diese Plattform entwickelte Software oder eine auf diesem Softwarestack / Plattform basierende Software einspielen
* Das Basissystem Betreut der Service Provider dieser muss auch für die Ausfallsicherheit und Skalierbarkeit sorgen.

---
## Beispiele
* Linux Apache Mysql PHP
* DNS
* Content Delivery Netzwerke

---
## Marktübersicht
* AppFog
* Heroku
* Openshift
* Google AppEngine

---
## Software as a Service
* Software wird meist in einem Mietmodell dem Kunden Angeboten
* Installation und Basisbetrieb wird vom Provider zur Verfügung gestellt
* Der Kunde kann seine Daten in die Software Plattform einspielen und eventuell geringfügige Änderungen im Verhalten der Software beeinflussen
* Die Administration erfolgt über ein Webinterface oder eine REST APIs

---
## Beispiele
* Microsoft Hosted Exchange (Office 365)
* Salesforce CRM Software

---
## Function as a Service
* Just Code to Execute
* Eventbasierte Ausführung
	* Webhooks
	* Scheduler

---
## Beispiele
* Lambda
* Azure Cloud Functions
* Openfaas
* Openwhisk

---
## Cloud Deployment Modelle
* Private Cloud ( Openstack / Vmware Cloud Director / Redhat Openshift)
* Community Cloud ( GRIDs / Oceanos in Griechenland)
* Public Cloud (Amazon / Goolge / Azure / Rackspace / DigitalOcian)
* Hybrid Cloud ( Openstack / Libcloud / Cisco Intercloud)

---
## Cloud Services Überblick
* Virtuelle Maschinen
* Container
* Content Delivery Netzwerke
* DNS Services
* LoadBalancer
* Datenspeicher (S3)
* Virtuelle Datencenter

---
## Infrastruktur as Code (libcloud)
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

cls = get_driver(Provider.RACKSPACE)
driver = cls('username', 'api key', region='iad')

sizes = driver.list_sizes()
images = driver.list_images()

size = [s for s in sizes if s.id == 'performance1-1'][0]

---
## Infrastruktur as Code (Ansible-Azure)
main.yml

```md
- name: Create virtual machine
  azure_rm_virtualmachine:
    resource_group: Testing
    name: testvm001
    vm_size: Standard_D1
    storage_account: testaccount001
    storage_container: testvm001
    storage_blob: testvm001.vhd
    admin_username: admin
    admin_password: Password!
    network_interfaces: testnic001
    image:
      offer: CentOS
      publisher: OpenLogic
      sku: '7.1'
      version: latest
```
---
## Achtung in der Cloud
#### (Nebelgefahr)
* Datenschutz Stichwort: Safe Habor / Patriot Act
* Verschlüsselung
* Vendor Lockin / Interoperatibillität
* eventuell bist du/deine Daten das Kapital (Freemium Modelle)
* SLA / Verträge ganau Lesen
* Zugriff über Netzwerk (DC in USA ???ms/Latenzen)
* Servicehotline ( Wo und Wann / Zeitzone)
* NSA und Geheimbrüder

---
## Links:
https://github.com/apache/incubator-openwhisk

---
# Fragen
